import json
from bs4 import BeautifulSoup
import requests
import nltk

mainDi={}
url="https://www.manheim.com/publicauctions/sales.do"

headers = {
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Host': "www.manheim.com",
    'cookie': "visitor_session_id=1556303109_7690_10.141.164.4; ak_bmsc=13636CA5CF6AF93693911D8685AFD8D7312C768DBF010000054DC35CCAF98130~pl5HFcF3re6g/1SP80W675tpQVB+Bq2eZq5LT9wbK74JZGE0G2o+B1qZsGAz3lAXgj9guB1NjgDpg4mQL0umVOb2GxBH4LwOddxhvEk9Au4L9L6OeikDizhrVfnKxQdPY5+M6fOUVe1S3eQQ8xkaNz/o+SyZVgB6JSsXy8subm5G+5RxFuOhA+k5pD2mbSAFeIEzYec8rbLR4P//tPjaiZDtSC1eena+iFpm6+Kbf8VZg=; export_redirect=true",
    'accept-encoding': "gzip, deflate",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

# hdr = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36'}
page_response = requests.get(url, timeout=50,headers=headers)
soup = BeautifulSoup(page_response.content, "html.parser")

def getSubMetaData(url):
    page_response = requests.get(url, timeout=50, headers=headers)
    soup = BeautifulSoup(page_response.content, "html.parser")
    imgs=soup.select('li[id^=image] img[src]')
    subMetaLi=[]
    subMetaDi={}
    imgsLiSm=[]
    imgsLiLg=[]
    for img in imgs:
        imgsLiSm.append(str(img['src']))
        imgsLiLg.append(str(img['src'].replace("/small","")))
    # subMetaDi['ImgListsSmall']=imgsLiSm
    subMetaDi['ImgListsLarge'] = imgsLiLg

    name=soup.select('div[id=vdpDetail] h3')
    subMetaDi['Product Title']=str(name[0].text)
    #firstColumn
    # sellerinfo = soup.select('div[class=firstColumn] h4')[0].text
    # print(sellerinfo)
    sellerLi=[]
    sellerli = soup.select('div[class=firstColumn] li')
    for seller in sellerli:
        sellerLi.append(str(seller.text))
    subMetaDi['Seller Information']=sellerLi
    # SecoundColumn
    # midCol = soup.select('div[class=middleColumn] h4')
    # locstr=midCol[0].text
    # saleStr=midCol[1].text
    midColVal = soup.select('div[class=middleColumn] li')
    locVal=midColVal[0].text.replace("  ", "").replace("\n", "").replace("\t", "")
    saleVal = midColVal[1].text.replace("  ", "").replace("\n", "").replace("\t", "")
    # print(locstr,locVal,saleStr,saleVal)
    subMetaDi['Location/State'] = str(locVal)
    subMetaDi['Sale Date'] = str(saleVal)

    # LastColumn
    # lastCol = soup.select('div[class=lastColumn] h4')
    # lanstr = lastCol[0].text
    lastColVal = soup.select('div[class=lastColumn] li')
    lanVal = lastColVal[0].text.replace("  ", "").replace("\n", "").replace("\t", "")
    rateVal = lastColVal[1].text.replace("  ", "").replace("\n", "").replace("\t", "")
    # print(lanstr, lanVal, rateVal)
    subMetaDi['Lane & Run'] = str(lanVal)
    subMetaDi['Rating'] = str(rateVal)


    #get Spec
    for tr in soup.select('div[id=vdpTab_detail-1] table tr'):
        specliStr=[]
        specliVal= []
        for th in tr.find_all('th'):
            specliStr.append(th.text)
        for td in tr.find_all('td'):
            specliVal.append(td.text)
        for index, (value1, value2) in enumerate(zip(specliStr, specliVal)):
            subMetaDi[str(value1)]=str(value2)
            # print(index, value1 + value2)
    subMetaLi.append(subMetaDi)
    return subMetaLi

def getMetaData(url):
    page_response = requests.get(url, timeout=50, headers=headers)
    soup = BeautifulSoup(page_response.content, "html.parser")
    i = 0
    subLi=[]
    for row in soup.select('table tbody tr'):
        try:
            subDi = {}
            i += 1
            li = []
            # if i > 2:
            #     break
            liCount = 0
            for td in row.find_all('td'):
                liCount += 1
                txt = td.text.replace("  ", "").replace("\n", "").replace("\t", "")
                if liCount == 1:
                    ilink = td.select('img[src]')[0]['src']
                    li.append(ilink)
                    alink = td.select('a[href]')[0]['href']
                    subDi['SubMetaData']=getSubMetaData("https://www.manheim.com/publicauctions/"+alink)

                li.append(txt)
        except:
            continue
        print("j",i,str(li[3]))
        subDi['Image Link']=str(li[0])
        subDi['Year'] = str(li[2])
        subDi['Make/Model (Click to view details)'] = str(li[3])
        subDi['Condition'] = str(li[4])
        subDi['	Odometer'] = str(li[5])
        subDi['	Color'] = str(li[6])
        subDi['VIN'] = str(li[7])
        subDi['	Ln-Run'] = str(li[8])
        subLi.append(subDi)
    # print(subLi)
    return subLi

i=0
mainLi=[]
for row in soup.select('table tbody tr'):
    try:
        i+=1
        li=[]
        # if i>2:
        #         break
        print("i",i)
        print("\n\n")
        liCount=0
        di = {}
        di['inventorySubData'] = []
        for td in row.find_all('td'):
                liCount+=1
                txt=td.text.replace("  ","").replace("\n","").replace("\t","")
                if liCount==3 and txt != 'Please Check Back Soon':
                        alink=td.select('a[href]')[0]['href']
                        di['inventorySubData']=getMetaData("https://www.manheim.com/publicauctions/"+alink)

                li.append(txt)
    except:
        continue

    di['Location']=li[0]
    di['Sale Date']=li[1]
    di['Inventory'] = li[2]
    di['Seller'] = li[3]
    di['Contact'] = li[4]
    mainLi.append(di)
mainDi['data']=mainLi
finalData=json.dumps(mainDi)

# print(finalData)
with open('data.json', 'w') as outfile:
    json.dump(mainDi, outfile)
