import mysql.connector
import json

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="Solitaire@786",
  database="manheim"
)

mycursor = mydb.cursor()



def storeData():
	with open('data.json') as json_file:  
		data = json.load(json_file)
		# data =json.loads(data)
		data= data['data']
		length =len(data)
		for i in range(0,length):
			# print(i)		
				
			values = (data[i]['Location'],data[i]['Sale Date'],data[i]['Inventory'],data[i]['Seller'],data[i]['Contact'])

			query1="INSERT IGNORE INTO mainPage (Location,SaleDate, Inventory,Seller,Contact) VALUES(%s, %s,%s,%s,%s)"
			mycursor.execute(query1,values)
			last_id =mycursor.lastrowid	
			# print(last_id)
			itemBank = []
			for isb in data[i]['inventorySubData']:
				print(i)
				imglinks =isb['SubMetaData'][0]['ImgListsLarge']
				imglinks = '|'.join(imglinks)			
				values1 = (isb['SubMetaData'][0]['Product Title'], \
					isb['SubMetaData'][0]['Location/State'], \
					isb['SubMetaData'][0]['Sale Date'], \
					isb['SubMetaData'][0]['Lane & Run'], \
					isb['SubMetaData'][0]['Rating'], \
					isb['SubMetaData'][0]['Year:'], \
					isb['SubMetaData'][0]['VIN:'], \
					isb['SubMetaData'][0]['Make:'], \
					isb['SubMetaData'][0]['Body Style:'], \
					isb['SubMetaData'][0]['Model:'], \
					isb['SubMetaData'][0]['Doors:'], \
					isb['SubMetaData'][0]['Trim Level:'], \
					isb['SubMetaData'][0]['Vehicle Type:'], \
					isb['SubMetaData'][0]['Odometer:'], \
					isb['SubMetaData'][0]['Title State:'], \
					isb['SubMetaData'][0]['Engine:'], \
					isb['SubMetaData'][0]['Title Status:'], \
					isb['SubMetaData'][0]['Transmission:'], \
					isb['SubMetaData'][0]['Interior Type:'], \
					isb['SubMetaData'][0]['Drive Train:'], \
					isb['SubMetaData'][0]['Top Type:'], \
					isb['SubMetaData'][0]['Exterior Color:'], \
					isb['SubMetaData'][0]['Stereo:'], \
					isb['SubMetaData'][0]['Interior Color:'], \
					isb['SubMetaData'][0]['Airbags:'], \
					isb['SubMetaData'][0]['Options:'], \
					isb['Image Link'], \
					isb['Make/Model (Click to view details)'], \
					isb['Condition'], \
					imglinks,\
					last_id)
				
				itemBank.append(values1)
			# print(itemBank)
			query2 ="insert ignore into submetadata(ProductTitle, LocationState, SaleDate, LaneRun, Rating, Year, VIN, Make, BodyStyle, Model, Doors, TrimLevel, VehicleType, Odometer, TitleState, Engine, TitleStatus, Transmission, InteriorType, DriveTrain, TopType, ExteriorColor, Sterion, InteriorColor, Airbags, Options, ImageLink, MakeModel, CurrentCondition,imgLinks, mainID)\
                 VALUES (%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s)"
			mycursor.executemany(query2,itemBank)
			
storeData()



