
import json
from bs4 import BeautifulSoup
import requests
from next_page import page_urls
from extractor import get_information
import pandas as pd


import pprint
mainDi={}
url="https://www.iaai.com/LiveAuctions"

headers = {
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Host': "www.iaai.com",
    'cookie': "visitor_session_id=1556303109_7690_10.141.164.4; ak_bmsc=13636CA5CF6AF93693911D8685AFD8D7312C768DBF010000054DC35CCAF98130~pl5HFcF3re6g/1SP80W675tpQVB+Bq2eZq5LT9wbK74JZGE0G2o+B1qZsGAz3lAXgj9guB1NjgDpg4mQL0umVOb2GxBH4LwOddxhvEk9Au4L9L6OeikDizhrVfnKxQdPY5+M6fOUVe1S3eQQ8xkaNz/o+SyZVgB6JSsXy8subm5G+5RxFuOhA+k5pD2mbSAFeIEzYec8rbLR4P//tPjaiZDtSC1eena+iFpm6+Kbf8VZg=; export_redirect=true",
    'accept-encoding': "gzip, deflate",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

def getSubData(url):
    page_response = requests.get(url, timeout=50, headers=headers)
    soup = BeautifulSoup(page_response.content, "html.parser")


page_response = requests.get(url, timeout=50,headers=headers)
soup = BeautifulSoup(page_response.content, "html.parser")
itemList=soup.select('div[id^=dv_]')
mainLi=[]
ii=0
for item in itemList:
    try:
        ii+=1
        print("Main",ii)
        # if ii>2:
        #     break
        di = {}
        for ul in item.select('ul'):
            try:
                lis = []
                for li in item.select('li'):
                    lis.append(li.text.replace("  ", ""))
                    if li.text=='View Sale List':
                        url="https://www.iaai.com/"+li.select('a[href]')[0]['href']
                        print(url)
            except Exception as e:
                print(e)
                continue
        di['Link']= url
        secondPageURL=page_urls(url)
        # di["SubLink"]=secondPageURL
        subSubData=[]
        cc=0
        for subURL in secondPageURL:
            try:
                cc+=1
                # if cc>3:
                #     break
                print("subURL",subURL)
                metaDataFinal=get_information(subURL)
                subSubData.append(metaDataFinal)
            except Exception as e:
                print(e)
                continue
        
        di["subMetaData"]=subSubData
        di['BRANCH']=lis[0].replace("\n","").replace("\t","").replace("\r","")
        di['BRANCH Loc'] = lis[1].replace("\n","").replace("\t","").replace("\r","")
        di['AUCTION TIME']=lis[2].replace("\n","").replace("\t","").replace("\r","")
        di['AUCTION START TIME']=lis[3].replace("\n","").replace("\t","").replace("\r","")
        di['# OF VEHICLES Total']=lis[4].replace("\n","").replace("\t","").replace("\r","")
        di['# OF VEHICLES Run & Drive'] = lis[5].replace("\n","").replace("\t","").replace("\r","")
        di['STATUS/ACTION']=lis[-2].replace("\n","").replace("\t","").replace("\r","")
        mainLi.append(di)
        with open("file_" + str(ii) + ".json", "w") as write_file:
            json.dump(di, write_file)
    except Exception as e:
        print(e)
        continue

# with open("data.json", "w") as write_file:
#     json.dump(mainLi, write_file)