import json
import time
import requests
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

options = Options()
options.add_argument('--no-sandbox')

options.add_argument('--headless')
options.add_argument('--disable-gpu')
options.add_argument("--remote-debugging-port=9259")


all_vehicle_data = {}


def get_information(url):
    url = url.replace('../', '')

    driver = webdriver.Chrome(chrome_options=options,
                          executable_path='./chromedriver')
    driver.get(url)
    soup = BeautifulSoup(driver.page_source, "html.parser")

    iaai =soup.find("div", {"class": "pd-condition-wrapper"})
    iaairow =iaai.findAll("div", {"class": "row flex"})
    vehicle_data ={}
    for row in iaairow:
        col5=row.find("div", {"class": "col-5 col-label"})
        key = col5.find("p").get_text()
        value =row.find("div",{"class":"col-7 col-value flex-self-end"}).get_text()
        value =value.replace("\n","")
        value =value.replace("\t","")
        value =value.replace("\r","")
        value =value.strip()
        vehicle_data[key] =value  
    mydivs = soup.find("div", {"class": "tabs tab-vehicle waypoint-trigger"})
    rowflex = mydivs.findAll("div", {"class": "row flex"})

    title =soup.find("h1",{"class":"pd-title-ymm"}).get_text()
    image =soup.find("img",{"id":"fullViewImg"})
 
    image=image['dimensionsallimagekeys'].split()
    image_list =image[0]
    from ast import literal_eval
    image_list=literal_eval(image_list)
    image_urls =[]
    for il in image_list:
      image_urls.append("https://vis.iaai.com/resizer?imageKeys="+il['K']+"&width=640&height=480")
    vehicle_data['image_urls']=image_urls
    vehicle_data["title"]=title
    vehicle_data["url"]=url
    for row in rowflex:
        col5 = row.find("div", {"class": "col-5 col-label"})
        key = col5.find("p").get_text()

        value = row.find("div", {"class": "col-7 col-value flex-self-end"}).get_text()
        value =value.replace("\n","")
        value =value.replace("\t","")
        value =value.replace("\r","")
        value =value.strip()
        vehicle_data[key] = value
    driver.close()

    return vehicle_data


# print(get_information("https://www.iaai.com/../Vehicle?itemID=32032805&RowNumber=69"))
